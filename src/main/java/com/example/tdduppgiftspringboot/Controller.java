package com.example.tdduppgiftspringboot;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class Controller {

    private final LoginService loginService;

    public Controller(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public String login(@RequestBody User user) throws Exception {
        return loginService.login(user);
    }
    @GetMapping("/account")
    public List<String> account(@RequestHeader("token") String token) throws Exception {
        if (!loginService.verifyToken(token))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return loginService.getAuthorities(token, "ACCOUNT");
    }
    @GetMapping("/provision")
    public List<String> provision(@RequestHeader("token") String token) throws Exception {
        if (!loginService.verifyToken(token))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return loginService.getAuthorities(token, "PROVISION_CALC");
    }
}
