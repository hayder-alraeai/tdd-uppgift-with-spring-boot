package com.example.tdduppgiftspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TddUppgiftSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(TddUppgiftSpringBootApplication.class, args);
    }

}
