package com.example.tdduppgiftspringboot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class ControllersTest {
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate testRestTemplate;
    @MockBean
    private LoginService loginService;


    @Test
    void login_success_test() throws Exception {
        User user = new User("hayder", "123", asList("READ"));
        when(loginService.login(any())).thenReturn("token");
        HttpEntity<User> entity = new HttpEntity<>(user);
        ResponseEntity<String> res = testRestTemplate.postForEntity("http://localhost:" + port + "/login", entity, String.class  );
        assertThat(res.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void login_fail_should_throw_exception_test() throws Exception {
        User user = new User("hayder", "123", asList("READ"));
        doThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong username or password")).when(loginService).login(any());
        HttpEntity<User> entity = new HttpEntity<>(user);
        ResponseEntity<String> res = testRestTemplate.postForEntity("http://localhost:" + port + "/login", entity, String.class  );
        assertThat(res.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    void account_success_test() throws Exception {
        when(loginService.verifyToken(any())).thenReturn(true);
        HttpHeaders token = new HttpHeaders();
        token.add("token", "token");
        HttpEntity<String> entity = new HttpEntity<>(token);
        ResponseEntity<String> res = testRestTemplate.exchange("http://localhost:" + port + "/account", HttpMethod.GET, entity, String.class);
        assertThat(res.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void account_fail_should_throw_exception_test() {
        when(loginService.verifyToken(any())).thenReturn(false);
        HttpHeaders token = new HttpHeaders();
        token.add("token", "token");
        HttpEntity<String> entity = new HttpEntity<>(token);
        ResponseEntity<String> res = testRestTemplate.exchange("http://localhost:" + port + "/account", HttpMethod.GET, entity, String.class);
        assertThat(res.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }
    @Test
    void provision_calc_success_test() throws Exception {
        when(loginService.verifyToken(any())).thenReturn(true);
        HttpHeaders token = new HttpHeaders();
        token.add("token", "token");
        HttpEntity<String> entity = new HttpEntity<>(token);
        ResponseEntity<String> res = testRestTemplate.exchange("http://localhost:" + port + "/provision", HttpMethod.GET, entity, String.class);
        assertThat(res.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void provision_calc_fail_should_throw_exception_test() {
        when(loginService.verifyToken(any())).thenReturn(false);
        HttpHeaders token = new HttpHeaders();
        token.add("token", "token");
        HttpEntity<String> entity = new HttpEntity<>(token);
        ResponseEntity<String> res = testRestTemplate.exchange("http://localhost:" + port + "/provision", HttpMethod.GET, entity, String.class);
        assertThat(res.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }
}
