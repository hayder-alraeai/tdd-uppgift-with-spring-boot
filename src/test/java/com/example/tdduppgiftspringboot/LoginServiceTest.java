package com.example.tdduppgiftspringboot;

import jdk.jfr.Description;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class LoginServiceTest {
    @InjectMocks
    private LoginService loginService;
    @Mock
    private PasswordHashingService passwordHashingService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Description(value = "should return a token")
    void login_test() throws Exception {
        when(passwordHashingService.verifyPassword(anyString(), anyString())).thenReturn(true);
        String token = loginService.login(new User("anna", "losen", asList("some Permissions")));
        assertThat(token, startsWith("ey"));
    }

    @Test
    @Description(value = "should fail and throw exeption")
    void login_fail() {
        when(passwordHashingService.verifyPassword(any(), any())).thenReturn(false);
        assertThrows(ResponseStatusException.class, () -> loginService.login(new User("nana", "sole", asList("some Permissions"))));
    }

    @Test
    @Description(value = "Take a token and success")
    void token_success_test() throws Exception {
        when(passwordHashingService.verifyPassword(anyString(), anyString())).thenReturn(true);
        String token = loginService.login(new User("anna", "losen", asList("some Permissions")));
        boolean isTokenValid = loginService.verifyToken(token);
        assertTrue(isTokenValid);
    }
    @Test
    @Description(value = "Take a token and fail")
    void token_fail_test() throws Exception {
        String token = loginService.generateToken("someUsername", asList("some auth"));
        boolean isTokenValid = loginService.verifyToken(token);
        assertFalse(isTokenValid);
    }

    @Test
    @Description(value = "should generate a token by giving it a username")
    void generate_token_test() {
        String token = loginService.generateToken("someUsername", asList("some auth"));
        assertThat(token, startsWith("ey"));
    }

    @Test
    @Description(value = "takes a token and resource name to get authorities")
    void read_authorities_test() throws Exception {
        String token = loginService.generateToken("anna", asList("READ"));
        List<String> authorities = loginService.getAuthorities(token, "ACCOUNT");
        assert(authorities).contains("READ");
    }
}

